const pg = require('pg');
const express = require('express');
const config = express();

let pgConfig = {
  database: 'TestDb',
  user:'postgres',
  password:'renzmaban',
  host:'127.0.0.1',
  ssl:false,
  max:20,
  min:3,
  idleTimeoutMills:1000

}

let pgPool = pg.Pool(pgConfig);

config.set('dbConfig',pgPool);

module.exports = config;