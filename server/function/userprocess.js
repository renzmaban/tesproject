const dbUsers = require('../database/dbUsers');
const utils = require('../utlis');


var registerUser = function(data,callback){
  console.log("data reg user",data);
  let dataArray =[data.username,data.password,data.name,data.email,data.Address];

  dbUsers.insertUser(dataArray,function(err,result){
    console.log("test",result);
    if(err) throw err;
  })

  return callback(false,"Success")
}

var verifyLogin = function(data,callback){
  console.log("login credentials",data);
  let dataArray = [data.username,data.password];
  dbUsers.verifyLogin(dataArray,function(err,result){
    if(err) throw err;
    if(result == null){
      console.log("null");
      return callback(false,"Username or Password is Incorrect")
    }
    else{
      return callback(false,result);
    }
    

      //get data from dbusers not sure if should still use string data to array from utils
  })
  
}

module.exports={
  registerUser:registerUser,
  verifyLogin:verifyLogin
}