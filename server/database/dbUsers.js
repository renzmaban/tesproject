const config = require('../config');
let pgConfig = config.get('dbConfig');

var insertUser = function(data,callback){
  pgConfig.connect((err,client,done) => {
      if(err) throw err;
      client.query('insert into users (username,password,name,email,address) values ($1,$2,$3,$4,$5)',data,(err,result)=>{
        done()
        if(err){
          return callback(err);
        }
        return callback(false,result.rows);
      })
  })
}

var verifyLogin = function(data,callback){
  pgConfig.connect((err,client,done)=>{
    if(err) throw err;
    client.query('select * from users where username=$1 and password=$2',data,(err,result)=>{
      done()
      if(err){
        return callback(err);
      }
      return callback(false,result.rows[0]);
    })
  })
}


module.exports ={
  insertUser:insertUser,
  verifyLogin:verifyLogin
}