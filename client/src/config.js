import * as web3 from 'web3';

const configServerHost = ''; //http://localhost:3002 // for any request to server
const host = 'http://ec2-34-226-217-83.compute-1.amazonaws.com';//for links
const profilesecret = 'bctphChangeProfileWallet';
const imagesecret = 'btcphimagetoken';
const emailSecret = "bctphEmailW@llet";
const identitySecret = 'btcphWalletidsec';
let web3Config;

if (typeof web3Config !== 'undefined') {
	web3Config = new web3(web3Config.currentProvider);
} else {
	web3Config = new web3(new web3.providers.HttpProvider('http://localhost:8545'));
}

export {configServerHost,profilesecret,imagesecret,emailSecret,identitySecret,web3Config,host}