import React,{Component} from 'react'
import LoginForm from './Form/loginForm'
import { Layout} from 'antd';
import '../style/style.css';
import axios from 'axios'
import {serverhost} from '../utils';

const { Content } = Layout;


export default class Login extends Component{
  constructor(props){
    super(props)
  }
  

  onSubmitHandler = (e,userEntries) =>{
    e.preventDefault();
    console.log("loginEntries",userEntries);
    axios({
      url:serverhost+'/login',
      method:'Post',
      data:userEntries
    }).then(data=>{
      console.log(data.data.result.name);
    }).catch(err=>{
      console.log("error",err);
    })

  }

  render(){
    return(
      <div> 
        <Content style={{ padding: "0 50px", "marginTop": "105px"}}>
          <LoginForm
            onSubmitHandler ={this.onSubmitHandler}
          />
        </Content>
      </div>
    )
  }


} 

