import React,{Component} from 'react'
import { Form, Input, Button } from 'antd';

export default class loginForm extends Component{
  constructor(props){
    super(props)
    this.state={
      login:{
        username:"",
        password:""
      }
    }
  }

  onChangeHandler = (e) =>{
    let {login} = this.state;
    switch(e.target.name){
      case'uUsername':
        this.setState({login:{...login,username:e.target.value}})
        console.log("username",this.state.login.username);
      break;
      case'uUpassword':
        this.setState({login:{...login,password:e.target.value}})
        console.log("password",this.state.login.password);
      break;
    }
  }


  render(){
    return(
      <div>
        <Form className='Login-Form' onSubmit= {(e)=> this.props.onSubmitHandler(e,this.state.login)}>
          <Input type="password" onChange={this.onChangeHandler} size="large" name='uUsername' placeholder="Input Username" />
          <Input onChange={this.onChangeHandler} size="large" name='uUpassword' placeholder="Input Password" />
          <Button type="primary" htmlType='submit' block>Submit</Button>
        </Form>
      </div>
    )
  }


}