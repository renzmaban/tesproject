import React,{Component} from 'react';
import { Form, Input, Button } from 'antd';

const {TextArea} = Input;

export default class registrationForm extends Component{
  constructor(props){
    super(props);
    this.state={
      userInput:{
        username:"",
        password:"",
        name:"",
        email:"",
        Address:""
      }
    };



  }

  onChangeHandler = (e) =>{
    let {userInput} = this.state;
    
    switch(e.target.name){
      case 'uUsername':
      console.log("val",e.target.value);
      this.setState({userInput:{userInput,username:e.target.value}})
      // this.setState({userInput:{...userInput,username:e.target.value}})
      console.log("username",userInput.username);
      break;
      case 'uPassword':
      this.setState({userInput:{...userInput,password:e.target.value}})
      console.log("password",this.state.userInput.password);
      break;
      case 'uName':
      this.setState({userInput:{...userInput,name:e.target.value}})
      console.log("name",this.state.userInput.name);
      break;
      case 'uEmail':
      this.setState({userInput:{...userInput,email:e.target.value}})
      console.log("email",this.state.userInput.email);
      break;
      case 'uAddress':
      this.setState({userInput:{...userInput,Address:e.target.value}})
      console.log("Address",this.state.userInput.Address);
      break;
    }

  }


  render(){
    console.log(this.props);
    return(
      <Form className = 'example-input' onSubmit={(e)=>this.props.onSubmitHandler(e, this.state.userInput)}>
        <h2>Register</h2>

        <Input onChange={this.onChangeHandler} size="large" name='uUsername' placeholder="Input Username" />
        <Input onChange={this.onChangeHandler} size="large" name='uPassword'type='password' placeholder="Input Password" />
        <Input onChange={this.onChangeHandler} size="large" name='uName' placeholder="Input Name" />
        <Input onChange={this.onChangeHandler} size="large" name='uEmail' placeholder="Input Email Address" />
        <TextArea onChange={this.onChangeHandler} rows={4} name='uAddress' placeholder="Input Address" autosize={{ minRows: 2, maxRows: 6 }} />
        <Button type="primary" htmlType='submit' block>Submit</Button>
      </Form>

    )
  }
}