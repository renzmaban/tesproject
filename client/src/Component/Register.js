import React, {Component} from 'react';
import RegistrationForm from './Form/registrationForm';
import { Layout} from 'antd';
import '../style/style.css';
import axios from 'axios'
import {serverhost} from '../utils';

const { Content } = Layout;

export default class Registration extends Component{
  constructor(props){
    super(props);
  }
  
  onSubmitHandler =(e,userEntries) =>{
    e.preventDefault();

    console.log("test data",userEntries);

    axios({
      url:serverhost+'/register',
      method:'POST',
      data:userEntries
    }).then(data=>{
      console.log(data.data);
    }).catch(err=>{
      console.log('the error',err);
    })
  }

  render(){
    return(
      <Content style={{ padding: "0 50px", "marginTop": "105px"}}>
      <RegistrationForm
        onSubmitHandler={this.onSubmitHandler}
      />
      </Content>
    )
  }

}