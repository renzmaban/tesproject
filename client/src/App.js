import React, { Component } from 'react';
import {Route, BrowserRouter, Switch, Redirect} from 'react-router-dom';

import logo from './logo.svg';
import Registration from './Component/Register';
import Login from './Component/Login';

class App extends Component {
  constructor(props){
    super(props);

  }


  render() {
    return (
    <BrowserRouter>
    <div className="App">
      <Switch>
        <Route exact path = "/Register" component={Registration}/>
        <Route exact path = "/Login" component={Login}/>
      </Switch>
      </div>
    </BrowserRouter>
    );
  }
}

export default App;
